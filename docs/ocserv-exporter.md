# Expose Prometheus metrics with ocserv-exporter

Author: Florian Domain 

## Scope

This recipe provides step by step instructions on how to run and scrape 
[ocserv-exporter](https://github.com/criteo/ocserv-exporter) from a Prometheus instance.
This exporter allows to gather various metrics on ocserv (number of connected users, failed authentication attempts, etc...).

## Platforms used for testing

This recipe was tested on the following platforms:
  
* CentOS 8 on amd64 architecture

## Prerequisites
 
 * ocserv with occtl enabled (`use-occtl = true` in the configuration)
 * a Prometheus instance (refer to the [official documentation](https://prometheus.io/docs/introduction/first_steps/) for its installation)
  
## Assumptions

This recipe assumes the reader has a basic understanding of a GNU/linux system and all commands are
run from a privileged user. It is recommended to login the system using root. If not possible,
execute "su root" or "sudo -s" to get highest privileges. The ocserv-exporter must run on the same system as ocserv.

## Installation

 1. Download the latest release of [ocserv-exporter](https://github.com/criteo/ocserv-exporter/releases) from ocserv server:
	```sh
	   curl -LO https://github.com/criteo/ocserv-exporter/releases/download/vx.x.x/ocserv-exporter_x.x.x_linux_amd64.tar.gz
	```
 2. Extract the archive:
	```sh
	   tar xvf ocserv-exporter_x.x.x_linux_amd64.tar.gz
	```
 3. Run the exporter
	```sh
	   ./ocserv-exporter -listen 0.0.0.0:8300
	```
 4. Check that you can get the metrics from the exporter
	```sh
	   curl -S http://127.0.0.1:8300/metrics
	```
 5. On the Prometheus instance, add the following block in the `scrape_configs` section of the configuration:
	```yaml
	   scrape_configs:
		- job_name: 'ocserv-exporter'
			scrape_interval: 30s
			static_configs:
				- targets:
					- x.x.x.x:8300  # The ocserv exporter's real ip:port
			metrics_path: /metrics
	```
 6. Restart Prometheus, ocserv-exporter metrics must have been scraped and available from the UI
